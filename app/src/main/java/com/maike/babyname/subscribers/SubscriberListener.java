package com.maike.babyname.subscribers;


interface SubscriberListener<T> {
    void onNext(T t);
    void onError(Throwable e);
    void onCancel();
    void onNetworkError(Throwable e);
}
