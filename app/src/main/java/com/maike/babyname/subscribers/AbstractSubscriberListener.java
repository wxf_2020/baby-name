package com.maike.babyname.subscribers;


import com.maike.babyname.utils.ToastUtils;

public abstract class AbstractSubscriberListener<T> implements SubscriberListener<T> {

    @Override
    public void onNext(T result) {

    }

    @Override
    public void onError(Throwable e) {
        ToastUtils.showShortToast(e.getMessage());

    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onNetworkError(Throwable e) {

    }
}
