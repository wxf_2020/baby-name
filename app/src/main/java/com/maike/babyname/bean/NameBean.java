package com.maike.babyname.bean;

public class NameBean {
    private String namePinYin;
    private String name;
    private String fiveElements;

    public NameBean(String namePinYin, String name, String fiveElements) {
        this.namePinYin = namePinYin;
        this.name = name;
        this.fiveElements = fiveElements;
    }

    public NameBean() {

    }


    public String getNamePinYin() {
        return namePinYin;
    }

    public void setNamePinYin(String namePinYin) {
        this.namePinYin = namePinYin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFiveElements() {
        return fiveElements;
    }

    public void setFiveElements(String fiveElements) {
        this.fiveElements = fiveElements;
    }
}
