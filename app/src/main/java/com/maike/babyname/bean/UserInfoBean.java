package com.maike.babyname.bean;

/**
 * Created by wangxuefei on 2019-10-22 19:13.
 */
public class UserInfoBean {


    /**
     * concernMe : 0
     * isShowCar : 1
     * hotline :
     * sex : 1
     * mobile :
     * vipType : 28
     * myConcern : 0
     * isAuth : 0
     * password : 96E79218965EB72C92A549DD5A330112
     * modifyTime : 1571742719000
     * balance : 0
     * createTime : 1571819730000
     * name : 13121655483
     * safetyScore : 0
     * isShowRole : 1
     * deposit : 0
     * hasCarAuth : 0
     * id : 41
     * status : 1
     */

    private int concernMe;//关注我的
    private int collect;//收藏
    private String hotline;//热线
    private int sex;//1 男的
    private String mobile;
    //1 普通会员 2 银卡  3  金卡   4 黑卡
    private int vipType;//会员类型
    private int myConcern;//我关注的
    private String password;
    private long modifyTime;
    private String balance;
    private String deposit;
    private long createTime;
    private String birthday;
    private String name;
    private String icon;
    private String address;
    private String regionStr;
    private String provinceId;
    private String cityId;
    private String areaId;
    private String countyId;
    private String autograph;

    private int identityStatus;//用户身份 0非推荐人  1推荐人
    private String recommendCode;//推荐码
    private String recommendCodeStr;// 显示推荐码的字符串

    // 0不显示  1显示
    private int isShowRole;//是否显示会员表示
    // 0不限时  1显示
    private int isShowCar;//显示车辆

    // 0未认证  1认证
    private int isCarAuth;//车辆是否认证
    private String brandImage;
    private String carAuthStatus;//车辆是否认证

    private int id;
    private int status;
    private int age;


    /**
     * isShowCar : 1
     * sex : 1
     * mobile : 13121655483
     * token : eyJhbGciOiJIUzI1NiIsIlR5cGUiOiJKd3QiLCJ0eXAiOiJKV1QifQ.eyJleHAiOjE1NzIzNDc2MTIsInVzZXJJZCI6NDF9.87VulRgKULLtY2YgNJm0SsayssjAmF28B_es5usEOhk
     * isAuth : 0
     * password : 111111
     * modifyTime : 1571742719000
     * createTime : 1571742719000
     * name : 13121655483
     * safetyScore : 0
     * isShowRole : 1
     * id : 41
     * status : 1
     */


    private String token;

    //0:未认证 1:认证中 2:认证
    private int userAuth;

    private int safetyScore;
    /**
     * birthday : -28800000
     * cityId : 676
     * balance : 0
     * countyId : 694
     * backdropImage :
     * mobile : 13121655483
     * provinceId : 675
     * isConcern : 0
     * deposit : 0
     */

    private int isConcern;


    public int getIdentityStatus() {
        return identityStatus;
    }

    public void setIdentityStatus(int identityStatus) {
        this.identityStatus = identityStatus;
    }

    public String getRecommendCode() {
        return recommendCode;
    }

    public void setRecommendCode(String recommendCode) {
        this.recommendCode = recommendCode;
    }

    public String getRecommendCodeStr() {
        return recommendCodeStr;
    }

    public void setRecommendCodeStr(String recommendCodeStr) {
        this.recommendCodeStr = recommendCodeStr;
    }

    public int getCollect() {
        return collect;
    }

    public void setCollect(int collect) {
        this.collect = collect;
    }

    public String getCarAuthStatus() {
        return carAuthStatus;
    }

    public void setCarAuthStatus(String carAuthStatus) {
        this.carAuthStatus = carAuthStatus;
    }

    public int getConcernMe() {
        return concernMe;
    }

    public void setConcernMe(int concernMe) {
        this.concernMe = concernMe;
    }

    public String getHotline() {
        return hotline;
    }

    public void setHotline(String hotline) {
        this.hotline = hotline;
    }

    public int getVipType() {
        return vipType;
    }

    public void setVipType(int vipType) {
        this.vipType = vipType;
    }

    public int getMyConcern() {
        return myConcern;
    }

    public void setMyConcern(int myConcern) {
        this.myConcern = myConcern;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRegionStr() {
        return regionStr;
    }

    public void setRegionStr(String regionStr) {
        this.regionStr = regionStr;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getCountyId() {
        return countyId;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId;
    }

    public String getAutograph() {
        return autograph;
    }

    public void setAutograph(String autograph) {
        this.autograph = autograph;
    }

    public int getIsCarAuth() {
        return isCarAuth;
    }

    public void setIsCarAuth(int isCarAuth) {
        this.isCarAuth = isCarAuth;
    }

    public String getBrandImage() {
        return brandImage;
    }

    public void setBrandImage(String brandImage) {
        this.brandImage = brandImage;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getIsShowCar() {
        return isShowCar;
    }

    public void setIsShowCar(int isShowCar) {
        this.isShowCar = isShowCar;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getUserAuth() {
        return userAuth;
    }

    public void setUserAuth(int userAuth) {
        this.userAuth = userAuth;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(long modifyTime) {
        this.modifyTime = modifyTime;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSafetyScore() {
        return safetyScore;
    }

    public void setSafetyScore(int safetyScore) {
        this.safetyScore = safetyScore;
    }

    public int getIsShowRole() {
        return isShowRole;
    }

    public void setIsShowRole(int isShowRole) {
        this.isShowRole = isShowRole;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIsConcern() {
        return isConcern;
    }

    public void setIsConcern(int isConcern) {
        this.isConcern = isConcern;
    }
}
