package com.maike.babyname.bean;

/**
 * 项目名称：BabyName
 * 类描述：
 * 创建人：wsk
 * 创建时间：2020/10/16 6:49 PM
 * 修改人：wsk
 * 修改时间：2020/10/16 6:49 PM
 * 修改备注：
 *
 * @author wsk
 */
public class AnalyseInfo {
    /**
     * 标题
     */
    public String title;
    /**
     * 内容介绍
     */
    public String content;
}
