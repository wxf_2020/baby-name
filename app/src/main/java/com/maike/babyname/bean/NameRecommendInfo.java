package com.maike.babyname.bean;

import java.util.List;

/**
 * 项目名称：BabyName
 * 类描述：
 * 创建人：wsk
 * 创建时间：2020/10/17 2:41 PM
 * 修改人：wsk
 * 修改时间：2020/10/17 2:41 PM
 * 修改备注：
 *
 * @author wsk
 */
public class NameRecommendInfo {
    public List<NameBean> nameList;
    /**
     * 评分
     */
    public int score;
    /**
     * 显示内容
     */
    public String content;
}
