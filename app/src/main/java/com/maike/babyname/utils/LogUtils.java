package com.maike.babyname.utils;

import android.util.Log;



public class LogUtils {

    /**
     * 是否启用日志输出功能
     */
//    public static final boolean ENABLE = BuildConfig.LOG_SWITCH;
    public static final boolean ENABLE = true;
    private static final String TAG = "BabyName";

    /**
     * 输出错误级别日志
     *
     * @param msg
     */
    public static void e(String msg) {
        if (ENABLE) {
            Log.e(TAG, msg);
        }
    }

    /**
     * 输出异常栈信息
     *
     * @param e
     */
    public static void e(Throwable e) {
        if (ENABLE) {
            Log.e(TAG, "出错啦！请查看下面的错误日志：", e);
        }
    }

    public static void Debug(String paramString) {
        if (ENABLE) {
            Log.d(TAG, paramString);
        }
    }

    public static void Http(String paramString) {
        if (ENABLE) {

            Log.e(TAG, paramString);
        }
    }
}
