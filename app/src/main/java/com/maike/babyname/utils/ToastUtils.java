package com.maike.babyname.utils;

import android.widget.Toast;

import com.maike.babyname.MyApplication;


public class ToastUtils {
    public static void showShortToast(String message) {
        Toast.makeText(MyApplication.getInstance(), message, Toast.LENGTH_SHORT).show();
    }

    public static void showLongToast(String message) {
        Toast.makeText(MyApplication.getInstance(), message, Toast.LENGTH_LONG).show();
    }
}
