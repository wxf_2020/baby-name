package com.maike.babyname.utils;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import com.maike.babyname.view.manager.MyGridLayoutManager;
import com.maike.babyname.view.manager.MyLinearLayoutManager;
import com.maike.babyname.view.manager.MyStaggeredGridLayoutManager;

/**
 * 项目名称：userchat
 * 类描述：
 * 创建人：wsk
 * 创建时间：2020/5/13 4:59 PM
 * 修改人：wsk
 * 修改时间：2020/5/13 4:59 PM
 * 修改备注：
 *
 * @author wsk
 */
public class LayoutManagerUtil {
    /**
     * 获取LinearLayoutManager，默认的ListView样式
     */
    @NonNull
    public static LinearLayoutManager getLinearLayoutManager(Context context) {
        return getLinearLayoutManager(context, true, false);
    }


    /**
     * 获取LinearLayoutManager
     *
     * @param isVertical 是否为垂直模式
     * @param reverseLayout 是否逆向布局（聊天界面数据从下往上就是逆向布局）
     */
    @NonNull
    public static LinearLayoutManager getLinearLayoutManager(Context context, boolean isVertical, boolean reverseLayout) {
        LinearLayoutManager layoutManager;
        if (isVertical) {
            layoutManager = new MyLinearLayoutManager(context, LinearLayoutManager.VERTICAL, reverseLayout);
        } else {
            layoutManager = new MyLinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, reverseLayout);
        }
        return layoutManager;
    }


    /**
     * 获取GridLayoutManager，默认的GridView样式
     *
     * @param spanCount 列数
     */
    @NonNull
    public static GridLayoutManager getGridLayoutManager(Context context, int spanCount) {
        return getGridLayoutManager(context, spanCount, true, false);
    }


    /**
     * 获取GridLayoutManager
     *
     * @param spanCount 如果是垂直方向,为列数。如果方向是水平的,为行数。
     * @param isVertical 是否为垂直模式
     * @param reverseLayout 是否逆向布局（聊天界面数据从下往上就是逆向布局）
     */
    @NonNull
    public static GridLayoutManager getGridLayoutManager(Context context, int spanCount, boolean isVertical, boolean reverseLayout) {
        GridLayoutManager layoutManager;
        if (isVertical) {
            layoutManager = new MyGridLayoutManager(context, spanCount, GridLayoutManager.VERTICAL, reverseLayout);
        } else {
            layoutManager = new MyGridLayoutManager(context, spanCount, GridLayoutManager.HORIZONTAL, reverseLayout);
        }
        return layoutManager;
    }


    /**
     * 获取StaggeredGridLayoutManager，默认的瀑布流样式
     *
     * @param spanCount 列数
     */
    @NonNull
    public static StaggeredGridLayoutManager getStaggeredGridLayoutManager(int spanCount) {
        return getStaggeredGridLayoutManager(spanCount, true);
    }


    /**
     * 获取StaggeredGridLayoutManager(瀑布流)
     *
     * @param spanCount 如果是垂直方向,为列数。如果方向是水平的,为行数。
     * @param isVertical 是否为垂直模式
     */
    @NonNull
    public static StaggeredGridLayoutManager getStaggeredGridLayoutManager(int spanCount, boolean isVertical) {
        StaggeredGridLayoutManager layoutManager;
        if (isVertical) {
            layoutManager = new MyStaggeredGridLayoutManager(spanCount, StaggeredGridLayoutManager.VERTICAL);
        } else {
            layoutManager = new MyStaggeredGridLayoutManager(spanCount, StaggeredGridLayoutManager.HORIZONTAL);
        }
        return layoutManager;
    }
}
