package com.maike.babyname.utils;

import android.app.Activity;
import android.content.Intent;

public class IntentUtils {
    public static void startWebViewIntent(Activity context, Class clazz, String title, String url) {
        Intent intent = new Intent(context, clazz);
        intent.putExtra("title", title);
        intent.putExtra("url", url);
        context.startActivity(intent);
//        context.overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_left_exit);
    }

    public static void startIntent(Activity context, Class clazz) {
        Intent intent = new Intent(context, clazz);
        context.startActivity(intent);
    }

    public static void startIntent(Activity context, Class clazz, String key, String value) {
        Intent intent = new Intent(context, clazz);
        intent.putExtra(key, value);
        context.startActivity(intent);
    }
}
