package com.maike.babyname.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

/**
 * Created by wangxuefei on 2020/4/17 10:32 AM.
 */
public class DrawableVerticalButton extends AppCompatButton {
    public DrawableVerticalButton(Context context) {
        super(context, null);
    }

    public DrawableVerticalButton(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public DrawableVerticalButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas = getTopCanvas(canvas);
        super.onDraw(canvas);
    }

    private Canvas getTopCanvas(Canvas canvas) {
        Drawable[] drawables = getCompoundDrawables();
        if (drawables == null) {
            return canvas;
        }


        Drawable drawable = drawables[0];// 左面的drawable


        if (drawable == null) {
            drawable = drawables[2];// 右面的drawable
        }

        if (drawable != null) {
            float textSize = getPaint().measureText(getText().toString()); //获取文本所占尺寸
            int drawWidth = drawable.getIntrinsicWidth(); //获取图标所占尺寸
            int drawPadding = getCompoundDrawablePadding(); //获取中间空余的尺寸
            float contentWidth = textSize + drawWidth + drawPadding; //计算当前所占尺寸
            int leftPadding = (int) (getWidth() - contentWidth);
            setPadding(0, 0, leftPadding, 0); // 直接贴到左边
            float dx = (getWidth() - contentWidth) / 2; //获取中心位置
            canvas.translate(dx, 0);// 整体往右移动
        } else {


            drawable = drawables[1];// 上面的drawable
            if (drawable == null) {
                drawable = drawables[3];// 下面的drawable
            }

            float textSize = getPaint().getTextSize();
            int iconSize = drawable.getIntrinsicHeight();
            int drawPadding = getCompoundDrawablePadding();

            float contentSize = textSize + iconSize + drawPadding;
            int topPadding = (int) (getHeight() - contentSize);
            setPadding(0, topPadding, 0, 0);
            float dy = (contentSize - getHeight()) / 2;
            canvas.translate(0, dy);//上移

        }
        return canvas;

    }
}
