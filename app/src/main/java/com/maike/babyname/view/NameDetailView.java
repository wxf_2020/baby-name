package com.maike.babyname.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.maike.babyname.R;
import com.maike.babyname.bean.NameBean;
import com.maike.babyname.ui.adapter.NameAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 * 项目名称：BabyName
 * 类描述：
 * 创建人：wsk
 * 创建时间：2020/10/17 2:49 PM
 * 修改人：wsk
 * 修改时间：2020/10/17 2:49 PM
 * 修改备注：
 *
 * @author wsk
 */
public class NameDetailView extends LinearLayout {
    @BindView(R.id.recyclerView) RecyclerView mRecyclerView;
    @BindView(R.id.tv_score) TextView mTvScore;
    @BindView(R.id.tv_top) TextView mTvTop;
    @BindView(R.id.tv_bottom) TextView mTvBottom;
    @BindView(R.id.tv_pin_top) TextView mTvPinTop;
    @BindView(R.id.tv_fan_top) TextView mTvFanTop;
    @BindView(R.id.tv_xing_top) TextView mTvXingTop;
    @BindView(R.id.tv_pin_bottom) TextView mTvPinBottom;
    @BindView(R.id.tv_fan_bottom) TextView mTvFanBottom;
    @BindView(R.id.tv_xing_bottom) TextView mTvXingBottom;


    public NameDetailView(Context context) {
        super(context);
    }


    public NameDetailView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }


    public NameDetailView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    {
        inflate(getContext(), R.layout.view_name_detail, this);
        ButterKnife.bind(this);
        List<NameBean> nameBeanList = new ArrayList<>();
        nameBeanList.add(new NameBean("li", "李", "[木]"));
        nameBeanList.add(new NameBean("yi", "意", "[土]"));
        nameBeanList.add(new NameBean("qing", "卿", "[木]"));
        NameAdapter nameAdapter = new NameAdapter(nameBeanList);
        mRecyclerView
                .setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        mRecyclerView.setAdapter(nameAdapter);
    }
}
