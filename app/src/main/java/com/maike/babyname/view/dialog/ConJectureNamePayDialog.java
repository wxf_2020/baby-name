package com.maike.babyname.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.maike.babyname.R;
import com.maike.babyname.utils.ToastUtils;
import com.maike.babyname.utils.UiUtils;
import com.maike.babyname.view.LineTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 测名字弹窗
 */
public class ConJectureNamePayDialog extends Dialog {

    @BindView(R.id.tv_new_price)
    TextView tvNewPrice;
    @BindView(R.id.tv_old_price)
    LineTextView tvOldPrice;
    @BindView(R.id.bt_ali_pay)
    Button btAliPay;
    @BindView(R.id.bt_wechat_pay)
    Button btWechatPay;
    @BindView(R.id.tv_buttom)
    TextView tvButtom;
    private Context mContext;

    public ConJectureNamePayDialog(@NonNull Context context) {
        super(context, R.style.dialog);
        mContext = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_conjecture_name_pay);
        ButterKnife.bind(this);
        Window window = getWindow();
        if (window != null) {
            window.getAttributes().width = ViewGroup.LayoutParams.MATCH_PARENT;
//            window.setWindowAnimations(R.style.dialog_bottom);
            window.setGravity(Gravity.CENTER);
        }
        initView();
        initListener();
    }

    private void initListener() {

    }

    private void initView() {
        String buttomStr = "已为<font color='#d60100'>%1$s</font>名用户提供专业测名";
        tvButtom.setText(Html.fromHtml(UiUtils.textFormat(buttomStr, "9128294")));

    }

    @OnClick({R.id.bt_ali_pay, R.id.bt_wechat_pay})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_ali_pay:
                ToastUtils.showShortToast("支付宝支付");
                break;
            case R.id.bt_wechat_pay:
                ToastUtils.showShortToast("微信支付");
                break;
        }
    }
}
