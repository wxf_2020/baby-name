package com.maike.babyname.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.maike.babyname.R;


public class DialogHelper {

    private static DialogHelper INSTANCE = new DialogHelper();
    private LayoutInflater layoutInflater;

    private DialogHelper() {
    }

    public static DialogHelper getInstance() {
        return INSTANCE;
    }

    private AlertDialog alertDialog;
    private View customView;

    public View getCustomView() {
        return customView;
    }

    private void showDialog(Context activity, int theme, View view, int width, int height, boolean outCancle, boolean cancleable) {
        dismiss();
        customView = view;
        if (alertDialog == null)
            alertDialog = new AlertDialog.Builder(activity, theme).setView(view).create();
        alertDialog.setCanceledOnTouchOutside(outCancle);
        alertDialog.setCancelable(cancleable);
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();
        if (width < 10) {
            width = FrameLayout.LayoutParams.WRAP_CONTENT;
        }
        if (height < 10) {
            height = FrameLayout.LayoutParams.WRAP_CONTENT;
        }
        view.setLayoutParams(new FrameLayout.LayoutParams(width, height));
    }


    public AlertDialog showLoading(Activity activity, String str, int width, int height, boolean outCancle, boolean cancleable) {
        View main = getInflater(activity).inflate(R.layout.loading_layout, null, false);
        WheelProgress wheelProgress = (WheelProgress) main.findViewById(R.id.wheel);
        wheelProgress.setSpeed(150);
        wheelProgress.setRotate(false);
        TextView text = main.findViewById(R.id.text);
        text.setText(str);

        showDialog(activity, R.style.MyDialogStyle, main, width, height, outCancle, cancleable);
        return alertDialog;
    }



    public void dismiss() {
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    public <T extends View> T find(View view, int id) {
        return (T) view.findViewById(id);
    }

    private DisplayMetrics displayMetrics;

    private DisplayMetrics getDM(Context activity) {
        if (displayMetrics == null) {
            WindowManager windowManager = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
            Display display = windowManager.getDefaultDisplay();
            displayMetrics = new DisplayMetrics();
            display.getMetrics(displayMetrics);
        }
        return displayMetrics;
    }

    private LayoutInflater getInflater(Context context) {
        if (layoutInflater == null) layoutInflater = LayoutInflater.from(context);
        return layoutInflater;
    }

    public static abstract class SelectDialogLinster {

        public void leftBtClick(Dialog materialDialog) {
        }


        public void rightBtClick(Dialog materialDialog) {
        }

        public void onSubmitClick(Dialog materialDialog, String floorName, String floorSort) {
        }


        public void rightBtClick(Dialog materialDialog, String title) {
        }

        public void rightBtClick(Dialog materialDialog, String projectNameStr, String customerNameStr, String customerPhoneStr, String addProjectProvinceCityStr) {
        }

        public void onFocusChange(EditText addProjectProvinceCity) {
        }


    }

}
