package com.maike.babyname.view.manager;

import android.content.Context;
import android.util.AttributeSet;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import com.maike.babyname.utils.LogUtils;

/**
 * 项目名称：userchat
 * 类描述：
 * 创建人：wsk
 * 创建时间：2020/5/13 5:03 PM
 * 修改人：wsk
 * 修改时间：2020/5/13 5:03 PM
 * 修改备注：
 *
 * @author wsk
 */
public class MyStaggeredGridLayoutManager extends StaggeredGridLayoutManager {
    public MyStaggeredGridLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    public MyStaggeredGridLayoutManager(int spanCount, int orientation) {
        super(spanCount, orientation);
    }


    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        try {
            super.onLayoutChildren(recycler, state);
        }catch (Exception e){
            LogUtils.e(e);
        }
    }
}
