package com.maike.babyname.view.manager;

import android.content.Context;
import android.util.AttributeSet;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.maike.babyname.utils.LogUtils;

/**
 * 项目名称：userchat
 * 类描述：
 * 创建人：wsk
 * 创建时间：2020/5/13 4:56 PM
 * 修改人：wsk
 * 修改时间：2020/5/13 4:56 PM
 * 修改备注：
 *
 * @author wsk
 */
public class MyLinearLayoutManager extends LinearLayoutManager {
    public MyLinearLayoutManager(Context context) {
        super(context);
    }


    public MyLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }


    public MyLinearLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        try {
            super.onLayoutChildren(recycler, state);
        }catch (Exception e){
            LogUtils.e(e);
        }
    }
}
