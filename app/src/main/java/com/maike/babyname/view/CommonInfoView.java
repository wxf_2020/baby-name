package com.maike.babyname.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.maike.babyname.R;

/**
 * 项目名称：BabyName
 * 类描述：
 * 创建人：wsk
 * 创建时间：2020/10/16 7:31 PM
 * 修改人：wsk
 * 修改时间：2020/10/16 7:31 PM
 * 修改备注：
 *
 * @author wsk
 */
public class CommonInfoView extends LinearLayout {
    @BindView(R.id.tv_top) TextView mTvTop;
    @BindView(R.id.tv_info) TextView mTvInfo;
    @BindView(R.id.rl_top_title) RelativeLayout mRlTopTitle;
    @BindView(R.id.tv_name) TextView mTvName;
    @BindView(R.id.tv_sex) TextView mTvSex;
    @BindView(R.id.tv_date_of_birth) TextView mTvDateOfBirth;
    @BindView(R.id.tv_place_of_birth) TextView mTvPlaceOfBirth;
    @BindView(R.id.tv_really_yang) TextView mTvReallyYang;
    @BindView(R.id.tv_bottom) TextView mTvBottom;
    @BindView(R.id.tv_have) TextView mTvHave;
    @BindView(R.id.rl_bottom_title) RelativeLayout mRlBottomTitle;


    public CommonInfoView(Context context) {
        super(context);
    }


    public CommonInfoView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }


    public CommonInfoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    {
        inflate(getContext(), R.layout.view_common_info, this);
        ButterKnife.bind(this);
    }
}
