package com.maike.babyname.view;

import android.app.Activity;
import android.content.Context;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.maike.babyname.R;
import com.maike.babyname.utils.UiUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 自定义页面标题栏
 */
public class TopView extends FrameLayout {

    private Context mContext;

    @BindView(R.id.rl_title_bar)
    RelativeLayout rlTitleBar;
    @BindView(R.id.img_esc)
    ImageView imgEsc;
    @BindView(R.id.txt_esc)
    TextView txtEsc;
    @BindView(R.id.txt_top_title)
    TextView txtTopTitle;
    @BindView(R.id.img_more)
    ImageView imgMore;
    @BindView(R.id.txt_more)
    TextView txtMore;

    protected OnEscClickListener onEscClickListener;
    protected OnMoreClickListener onMoreClickListener;

    public TopView(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }

    public TopView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.view_top, this);
        ButterKnife.bind(this, view);
    }

    @OnClick({R.id.img_esc, R.id.img_more, R.id.txt_more, R.id.txt_esc})
    void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_esc:
                if (onEscClickListener != null)
                    onEscClickListener.clickEsc(v);
                else
                    ((Activity) mContext).finish();
                break;
            case R.id.txt_esc:
                if (onEscClickListener != null)
                    onEscClickListener.clickEsc(v);
                else
                    ((Activity) mContext).finish();
                break;
            case R.id.img_more:
                if (onMoreClickListener != null)
                    onMoreClickListener.clickMore(v);
                break;
            case R.id.txt_more:
                if (onMoreClickListener != null)
                    onMoreClickListener.clickMore(v);
                break;
        }
    }

    /**
     * 隐藏返回键
     */
    public void hideEsc() {
        imgEsc.setVisibility(View.GONE);
    }

    /**
     * 显示返回键
     */
    public void showEsc() {
        imgEsc.setVisibility(View.VISIBLE);
    }

    /**
     * 替换返回键图标
     */
    public void replaceEscIcon(int icon) {
        imgEsc.setImageResource(icon);
    }

    /**
     * 设置返回键监听
     */
    public void setOnEscClickListener(OnEscClickListener onEscClickListener) {
        this.onEscClickListener = onEscClickListener;
    }

    /**
     * 设置标题栏背景色
     *
     * @param color
     */
    public void setTitleBarBg(int color) {
        this.rlTitleBar.setBackgroundColor(mContext.getResources().getColor(color));
    }

    /**
     * 设置标题
     *
     * @param title
     */
    public void setTitle(String title) {
        txtTopTitle.setText(title);
    }

    /**
     * 设置标题
     *
     * @param title
     */
    public void setTitle(int title) {
        txtTopTitle.setText(title);
    }

    /**
     * 设置标题
     *
     * @param isBold 是否加粗
     */
    public void setTitleBold(boolean isBold) {
        TextPaint tp = txtTopTitle.getPaint();

        tp.setFakeBoldText(true);
    }

    /**
     * 设置标文字颜色
     *
     * @param color
     */
    public void setTitleTextColor(int color) {
        txtTopTitle.setTextColor(mContext.getResources().getColor(color));
    }

    /**
     * 设置标文字大小
     *
     * @param textSizeSP
     */
    public void setTitleTextSize(int textSizeSP) {
        txtTopTitle.setTextSize(textSizeSP);
    }

    /**
     * 替换更多图标
     *
     * @param iconId
     */
    public void replaceMoreIcon(int iconId) {
        imgMore.setImageResource(iconId);
    }

    /**
     * 设置padding值
     *
     * @param padding
     */
    public void setMoreIconPadding(int padding) {
        imgMore.setPadding(UiUtils.dp2px(mContext, padding), UiUtils.dp2px(mContext, padding), UiUtils.dp2px(mContext, padding), UiUtils.dp2px(mContext, padding));
    }

    /**
     * 替换更多图标
     *
     * @param iconId
     */
//    public void replaceMoreIconColor(int iconId) {
//        imgMore.tint(iconId);
//    }

    /**
     * 设置更多图标监听
     *
     * @param onMoreClickListener
     */
    public void showMoreImg(OnMoreClickListener onMoreClickListener) {
        this.onMoreClickListener = onMoreClickListener;
        imgMore.setVisibility(View.VISIBLE);
    }

    /**
     * 设置更多文字
     *
     * @param title
     */
    public void setMoreTxt(String title) {
        txtMore.setText(title);
    }

    /**
     * 设置返回文字
     *
     * @param title
     */
    public void setEscTxt(String title) {
        txtEsc.setText(title);
        txtEsc.setVisibility(View.VISIBLE);
        imgEsc.setVisibility(View.GONE);
    }

    /**
     * 设置更多文字
     *
     * @param title
     */
    public void setMoreTxt(int title) {
        txtMore.setText(title);
    }

    /**
     * 设置返回文字
     *
     * @param title
     */
    public void setEscTxt(int title) {
        txtEsc.setText(title);
        txtEsc.setVisibility(View.VISIBLE);
        imgEsc.setVisibility(View.GONE);
    }

    /**
     * 设置更多文字颜色
     *
     * @param color
     */
    public void setMoreTxtColor(int color) {
        txtMore.setTextColor(mContext.getResources().getColor(color));
    }

    /**
     * 设置返回文字颜色
     *
     * @param color
     */
    public void setEscTxtColor(int color) {
        txtEsc.setTextColor(mContext.getResources().getColor(color));
    }

    /**
     * 设置更多文字大小
     *
     * @param textSizeSP
     */
    public void setMoreTextSize(int textSizeSP) {
        txtMore.setTextSize(textSizeSP);
    }

    /**
     * 设置返回文字大小
     *
     * @param textSizeSP
     */
    public void setEscTextSize(int textSizeSP) {
        txtEsc.setTextSize(textSizeSP);
    }

    /**
     * 设置更多文字监听
     *
     * @param onMoreClickListener
     */
    public void showMoreTxt(OnMoreClickListener onMoreClickListener) {
        this.onMoreClickListener = onMoreClickListener;
        txtMore.setVisibility(View.VISIBLE);
    }

    /**
     * 设置返回文字监听
     *
     * @param onEscClickListener
     */
    public void showEscTxt(OnEscClickListener onEscClickListener) {
        this.onEscClickListener = onEscClickListener;
        txtEsc.setVisibility(View.VISIBLE);
        imgEsc.setVisibility(View.GONE);
    }

    public interface OnEscClickListener {
        void clickEsc(View var1);
    }

    public interface OnMoreClickListener {
        void clickMore(View var1);
    }

}
