package com.maike.babyname.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;

import com.maike.babyname.R;
import com.maike.babyname.utils.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 推荐名字弹窗
 */
public class RecommendNamePayDialog extends Dialog {
    @BindView(R.id.bt_commit)
    Button btCommit;
    @BindView(R.id.radio_button_1)
    RadioButton radioButton1;
    @BindView(R.id.radio_button_2)
    RadioButton radioButton2;
    @BindView(R.id.radio_button_3)
    RadioButton radioButton3;
    @BindView(R.id.radio_group)
    RadioGroup radioGroup;
    private Context mContext;

    public RecommendNamePayDialog(@NonNull Context context) {
        super(context, R.style.dialog);
        mContext = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_recommend_name_pay);
        ButterKnife.bind(this);
        Window window = getWindow();
        if (window != null) {
            window.getAttributes().width = ViewGroup.LayoutParams.MATCH_PARENT;
//            window.setWindowAnimations(R.style.dialog_bottom);
            window.setGravity(Gravity.CENTER);
        }
        initView();
        initListener();
    }

    private void initListener() {
        radioButton1.setChecked(true);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int resId) {
                if (resId == radioButton1.getId()) {
                    radioButton1.setTextColor(0xffffffff);
                    radioButton2.setTextColor(0xff000000);
                    radioButton3.setTextColor(0xff000000);
                } else if (resId == radioButton2.getId()) {
                    radioButton1.setTextColor(0xff000000);
                    radioButton2.setTextColor(0xffffffff);
                    radioButton3.setTextColor(0xff000000);
                } else {
                    radioButton1.setTextColor(0xff000000);
                    radioButton2.setTextColor(0xff000000);
                    radioButton3.setTextColor(0xffffffff);
                }

            }
        });
    }

    private void initView() {
    }

    @OnClick(R.id.bt_commit)
    public void onViewClicked() {
        ToastUtils.showShortToast("唤起支付");
    }
}
