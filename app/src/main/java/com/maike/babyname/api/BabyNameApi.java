package com.maike.babyname.api;


import com.maike.babyname.bean.UserInfoBean;

import java.util.HashMap;
import com.maike.babyname.network.HttpResult;

import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by wangxuefei on 2019-10-22 18:31.
 */
public interface BabyNameApi {


    /**
     * 登陆
     *
     * @param maps
     * @return
     */
    @GET("user/login")
    Observable<HttpResult<UserInfoBean>> login(@QueryMap HashMap<String, Object> maps);



}
