package com.maike.babyname.mvp.model;



import com.maike.babyname.api.BabyNameApi;
import com.maike.babyname.base.BaseModel;

import java.util.HashMap;

import rx.Observer;
import rx.Subscription;

/**
 * Created by happy on 2019-08-24 11:10.
 */
public class LoginModel extends BaseModel<BabyNameApi> {
    public LoginModel() {
        super(BabyNameApi.class);
    }


    public Subscription login(Observer observer, HashMap<String, Object> map) {
        return mHttpMethods.toSubscriber(mAPI.login(map), observer);
    }


}
