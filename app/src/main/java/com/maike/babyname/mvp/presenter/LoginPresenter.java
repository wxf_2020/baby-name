package com.maike.babyname.mvp.presenter;

import android.os.CountDownTimer;
import android.widget.TextView;

import com.maike.babyname.network.HttpResult;

import com.maike.babyname.base.BasePresenter;
import com.maike.babyname.bean.UserInfoBean;
import com.maike.babyname.subscribers.AbstractSubscriberListener;
import com.maike.babyname.subscribers.ProgressSubscriber;
import com.maike.babyname.ui.activity.LoginActivity;
import com.maike.babyname.mvp.model.LoginModel;

import java.util.HashMap;

/**
 * Created by admin on 2019-08-24 11:28.
 */
public class LoginPresenter extends BasePresenter<LoginActivity, LoginModel> {
    LoginActivity activity;
    private CountDownTimer mTimer;
    private TextView txtGetVercode;

    public LoginPresenter(LoginActivity view, TextView txtGetVercode) {
        super(view);
        mModel = new LoginModel();
        this.activity = view;
        this.txtGetVercode = txtGetVercode;

    }


    public void login(HashMap<String, Object> map, int loginFinishType) {
        addToCompose(mModel.login(new ProgressSubscriber<HttpResult<UserInfoBean>>(new AbstractSubscriberListener<HttpResult<UserInfoBean>>() {
            @Override
            public void onNext(HttpResult<UserInfoBean> result) {
                super.onNext(result);

                activity.finish();

            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);

            }

            @Override
            public void onCancel() {
                super.onCancel();

            }
        }, mView), map));
    }


}
