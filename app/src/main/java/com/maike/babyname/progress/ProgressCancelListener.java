package com.maike.babyname.progress;



public interface ProgressCancelListener {
    void onCancelProgress();
}
