package com.maike.babyname.network;

import java.util.HashMap;


public class MyRequestParams {
    public static MyRequestParams requestParams;
    public static HashMap<String, Object> paramMap;

    public static MyRequestParams getInstance() {
//        if (null == paramMap) {
        paramMap = new HashMap<>();
//        }
        if (null == requestParams) {
            requestParams = new MyRequestParams();
        }
//        paramMap.clear();
        return requestParams;
    }

    public MyRequestParams addParam(String key, Object objVal) {
        paramMap.put(key, objVal);
        return requestParams;
    }
}
