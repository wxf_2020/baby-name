package com.maike.babyname.network;

/**
 * 响应对象
 */

public class HttpResult<T> implements java.io.Serializable {


    public int status = -1;

    public String message;


    private T data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
