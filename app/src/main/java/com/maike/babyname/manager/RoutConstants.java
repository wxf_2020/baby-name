package com.maike.babyname.manager;

/**
 * 项目名称：AndroidLibs
 * 类描述：
 * 创建人：wsk
 * 创建时间：2019-12-25 16:17
 * 修改人：wsk
 * 修改时间：2019-12-25 16:17
 * 修改备注：
 *
 * @author wsk
 */
public interface RoutConstants {


    interface Activity {

        /**
         * 引导页
         */
        String SPLASH_ACTIVITY = "/activity/SplashActivity";

        /**
         * 登录页
         */
        String LOGIN_ACTIVITY = "/activity/LoginActivity";

        /**
         * 主页
         */
        String MAIN_ACTIVITY = "/activity/MainActivity";
        /**
         * 常见问题
         */
        String HELP_FEED_BACK_ACTIVITY = "/activity/HelpFeedBackActivity";
        /**
         * 更多设置
         */
        String SETTING_ACTIVITY = "/activity/SettingActivity";
        /**
         * 取名分析
         */
        String BE_NAME_ANALYSE_ACTIVITY = "/activity/BeNameAnalyseActivity";

        /**
         * 测名结果
         */
        String CONJECTURE_RESULT_ACTIVITY = "/activity/ConjectureResultActivity";
        /**
         *  取名详情
         */
        String BE_NAME_DETAILS_ACTIVITY = "/activity/BeNameDetailsActivity";

    }

    interface Fragment {
        /**
         * 您的信息页面
         */
        String YOUR_INFO_FRAGMENT = "/fragment/YourInfoFragment";
        /**
         * 名字推荐
         */
        String NAME_RECOMMEND_FRAGMENT = "/fragment/NameRecommendFragment";

    }


}
