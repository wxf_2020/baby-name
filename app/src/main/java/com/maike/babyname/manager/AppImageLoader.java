package com.maike.babyname.manager;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.maike.babyname.base.GlideApp;
import com.maike.babyname.view.BlurTransformation;


/**
 * Created by Terry on 1/16/17.
 */

public class AppImageLoader {
    private static final AppImageLoader ourInstance = new AppImageLoader();

    public static AppImageLoader getInstance() {
        return ourInstance;
    }

    private AppImageLoader() {
    }

    public void displayImage(String url, ImageView imageView) {
        if (TextUtils.isEmpty(url)) {
            url = "";
        }
        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        GlideApp.with(imageView.getContext())
                .load(url)
                .centerCrop()
                .priority(Priority.HIGH)
                .into(imageView);
    }

    public void displayFitCenterImage(String url, ImageView imageView) {
        if (TextUtils.isEmpty(url)) {
            url = "";
        }
        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        GlideApp.with(imageView.getContext())
                .load(url)
                .fitCenter()
                .priority(Priority.HIGH)
                .into(imageView);
    }

    public void displayImage(String url, ImageView imageView, int defaultImg) {
        if (TextUtils.isEmpty(url)) {
            url = "";
        }
        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        GlideApp.with(imageView.getContext())
                .asBitmap()
                .load(url)
                .placeholder(defaultImg)
                .centerCrop()
                .error(defaultImg)
                .priority(Priority.HIGH)
                .transform(new CenterCrop())
                .into(imageView);
    }

    public void displayImage(int url, ImageView imageView) {
        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        GlideApp.with(imageView.getContext())
                .load(url)
                .priority(Priority.HIGH)
                .into(imageView);
    }

    public void displayRoundBannerImage(Context mContent, String url, ImageView imageView, int placeholderResId, int radius) {
        // 设置图片圆角角度
        RoundedCorners roundedCorners = new RoundedCorners(radius);
        // 通过RequestOptions扩展功能
        RequestOptions options = RequestOptions.bitmapTransform(roundedCorners).override(imageView.getWidth(), imageView.getHeight());
        GlideApp.with(mContent)
                .load(url)
                .placeholder(placeholderResId)
                .apply(options)
                .into(imageView);
    }

    public void displayBlurImage(Context mContent, int url, ImageView imageView, int radius, int sampling) {
        // 设置图片圆角角度
        BlurTransformation roundedCorners = new BlurTransformation(radius, sampling);
        // 通过RequestOptions扩展功能
        RequestOptions options = RequestOptions.bitmapTransform(roundedCorners);
        GlideApp.with(mContent)
                .load(url)
                .apply(options)
                .into(imageView);
    }

    public void displayBlurImage(Context mContent, String url, ImageView imageView, int radius, int sampling) {
        // 设置图片圆角角度
        BlurTransformation roundedCorners = new BlurTransformation(radius, sampling);
        // 通过RequestOptions扩展功能
        RequestOptions options = RequestOptions.bitmapTransform(roundedCorners);
        GlideApp.with(mContent)
                .load(url)
                .apply(options)
                .into(imageView);
    }

    public void displayLocalSDCardImage(String url, ImageView imageView) {
        if (TextUtils.isEmpty(url)) {
            url = "";
        }
        Uri uri = Uri.parse("file://" + url);
        if (imageView == null || imageView.getContext() == null) {
            return;
        }
        GlideApp.with(imageView.getContext())
                .load(uri)
                .priority(Priority.HIGH)
                .into(imageView);
    }
}
