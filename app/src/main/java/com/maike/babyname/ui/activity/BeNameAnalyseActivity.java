package com.maike.babyname.ui.activity;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.OnClick;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.maike.babyname.R;
import com.maike.babyname.base.BaseActivity;
import com.maike.babyname.manager.RoutConstants;
import com.maike.babyname.view.CommonAdapter;
import com.maike.babyname.view.TopView;
import java.util.ArrayList;
import java.util.List;

/**
 * 项目名称：BabyName
 * 类描述：取名分析
 * 创建人：wsk
 * 创建时间：2020/10/15 8:57 PM
 * 修改人：wsk
 * 修改时间：2020/10/15 8:57 PM
 * 修改备注：
 *
 * @author wsk
 */
@Route(path = RoutConstants.Activity.BE_NAME_ANALYSE_ACTIVITY)
public class BeNameAnalyseActivity extends BaseActivity {
    @BindView(R.id.viewPager) ViewPager mViewPager;
    @BindView(R.id.top_view) TopView mTopView;
    @BindView(R.id.tv_give_name) TextView mTvGiveName;
    @BindView(R.id.tv_conjecture_name) TextView mTvConjectureName;
    @BindView(R.id.rl_title_bar) RelativeLayout mRlTitleBar;
    private List<Fragment> mFragmentList = new ArrayList<>();


    @Override
    protected int getLayoutId() {
        return R.layout.act_bename_analyse;
    }


    @Override
    protected void initView() {
        mTopView.setTitle("取名分析");
    }


    @Override
    protected void initData() {
        Fragment yourInfoFragment = (Fragment) ARouter.getInstance().build(RoutConstants.Fragment.YOUR_INFO_FRAGMENT)
                                                .navigation();
        Fragment nameRecommendFragment = (Fragment) ARouter.getInstance()
                                                      .build(RoutConstants.Fragment.NAME_RECOMMEND_FRAGMENT)
                                                      .navigation();
        mFragmentList.add(yourInfoFragment);
        mFragmentList.add(nameRecommendFragment);
        CommonAdapter commonAdapter= new CommonAdapter(getSupportFragmentManager(),mFragmentList);
        mViewPager.setAdapter(commonAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                setFragmentIndicator(position);
            }
        });
    }


    @OnClick({R.id.tv_give_name, R.id.tv_conjecture_name})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_give_name:
                setFragmentIndicator(0);
                break;
            case R.id.tv_conjecture_name:
                setFragmentIndicator(1);
                break;
        }
    }

    public void setFragmentIndicator(int which) {
        switch (which) {
            case 0:
                mTvGiveName.setTextColor(0xffffffff);
                mTvConjectureName.setTextColor(0xff010101);
                mTvGiveName.setBackgroundResource(R.drawable.ic_main_home_bar);
                mTvConjectureName.setBackgroundResource(0);
                mViewPager.setCurrentItem(0);
                break;
            case 1:
                mTvGiveName.setTextColor(0xff010101);
                mTvConjectureName.setTextColor(0xffffffff);
                mTvGiveName.setBackgroundResource(0);
                mTvConjectureName.setBackgroundResource(R.drawable.ic_main_home_bar);
                mViewPager.setCurrentItem(1);
                break;
        }
    }


    @Override
    protected void initListener() {

    }

}
