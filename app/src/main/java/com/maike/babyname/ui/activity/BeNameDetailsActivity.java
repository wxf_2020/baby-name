package com.maike.babyname.ui.activity;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.maike.babyname.R;
import com.maike.babyname.base.BaseListActivity;
import com.maike.babyname.bean.AnalyseInfo;
import com.maike.babyname.manager.RoutConstants;
import com.maike.babyname.ui.adapter.YourInfoAdapter;
import com.maike.babyname.utils.LayoutManagerUtil;
import com.maike.babyname.view.NameDetailView;
import com.maike.babyname.view.TopView;
import java.util.ArrayList;
import java.util.List;

/**
 * 项目名称：BabyName
 * 类描述：
 * 创建人：wsk
 * 创建时间：2020/10/17 2:34 PM
 * 修改人：wsk
 * 修改时间：2020/10/17 2:34 PM
 * 修改备注：
 *
 * @author wsk
 */
@Route(path = RoutConstants.Activity.BE_NAME_DETAILS_ACTIVITY)
public class BeNameDetailsActivity extends BaseListActivity {
    @BindView(R.id.recyclerView) RecyclerView mRecyclerView;
    @BindView(R.id.top_view) TopView mTopView;
    private YourInfoAdapter mYourInfoAdapter;
    private List<AnalyseInfo> mAnalyseInfoList;
    private NameDetailView mNameDetailView;


    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return LayoutManagerUtil.getLinearLayoutManager(this);
    }


    @Override
    public int getLayoutId() {
        return R.layout.act_bename_details;
    }


    @Override
    protected void initData() {
        super.initData();
        mAnalyseInfoList = new ArrayList<>();
        loadData(false);
    }


    @Override
    protected BaseQuickAdapter getBaseQuickAdapter() {
        mYourInfoAdapter = new YourInfoAdapter(mList);
        return mYourInfoAdapter;
    }


    @Override
    protected void requestData(boolean getMore) {
        for (int i = 0; i < 3; i++) {
            AnalyseInfo analyseInfo = new AnalyseInfo();
            analyseInfo.title = "生肖用字分析";
            analyseInfo.content = "    宝宝生肖属相为鼠，起名宜有米、豆、鱼、艹、金、玉、人、木、月、田，增加宝宝运势，以保完事顺利。";
            mAnalyseInfoList.add(analyseInfo);
        }
        onDataSuccess(mAnalyseInfoList);
    }


    @Override
    protected View getHeaderView() {
        mNameDetailView = new NameDetailView(this);
        return mNameDetailView;
    }


    @Override
    protected void initView() {
        mTopView.setTitle("取名分析");
    }


    @Override
    protected void initListener() {

    }
}
