package com.maike.babyname.ui.fragment;

import android.view.LayoutInflater;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.maike.babyname.R;
import com.maike.babyname.base.BaseListFragment;
import com.maike.babyname.bean.AnalyseInfo;
import com.maike.babyname.manager.RoutConstants;
import com.maike.babyname.ui.adapter.YourInfoAdapter;
import com.maike.babyname.utils.LayoutManagerUtil;
import com.maike.babyname.view.CommonInfoView;
import java.util.ArrayList;
import java.util.List;

/**
 * 项目名称：BabyName
 * 类描述：您的信息
 * 创建人：wsk
 * 创建时间：2020/10/16 5:59 PM
 * 修改人：wsk
 * 修改时间：2020/10/16 5:59 PM
 * 修改备注：
 *
 * @author wsk
 */
@Route(path = RoutConstants.Fragment.YOUR_INFO_FRAGMENT)
public class YourInfoFragment extends BaseListFragment {
    @BindView(R.id.recyclerView) RecyclerView mRecyclerView;
    private YourInfoAdapter mYourInfoAdapter;
    private List<AnalyseInfo> mAnalyseInfoList;
    private CommonInfoView mCommonInfoView;


    @Override
    protected View createView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.fragment_name_recommend, null);
    }


    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return LayoutManagerUtil.getLinearLayoutManager(getContext());
    }


    @Override
    protected void initData() {
        super.initData();
        mAnalyseInfoList = new ArrayList<>();
        loadData(false);
    }


    @Override
    protected BaseQuickAdapter getBaseQuickAdapter() {
        mYourInfoAdapter = new YourInfoAdapter(mList);
        return mYourInfoAdapter;
    }


    @Override
    protected void requestData(boolean getMore) {
        for (int i = 0; i < 5; i++) {
            AnalyseInfo analyseInfo = new AnalyseInfo();
            analyseInfo.title = "生肖用字分析";
            analyseInfo.content = "    宝宝生肖属相为鼠，起名宜有米、豆、鱼、艹、金、玉、人、木、月、田，增加宝宝运势，以保完事顺利。";
            mAnalyseInfoList.add(analyseInfo);
        }
        onDataSuccess(mAnalyseInfoList);
    }

    @Override
    protected View getHeaderView() {
        mCommonInfoView = new CommonInfoView(getContext());
        return mCommonInfoView;
    }


    @Override
    protected void initListenter() {

    }


    @Override
    protected void initView() {

    }
}
