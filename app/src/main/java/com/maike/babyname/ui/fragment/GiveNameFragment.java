package com.maike.babyname.ui.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.alibaba.android.arouter.launcher.ARouter;
import com.maike.babyname.R;
import com.maike.babyname.base.BaseFragment;
import com.maike.babyname.manager.RoutConstants;

/**
 * 取名字
 */
public class GiveNameFragment extends BaseFragment {
    @BindView(R.id.et_first_name)
    EditText etFirstName;
    @BindView(R.id.tv_sex_man)
    TextView tvSexMan;
    @BindView(R.id.tv_sex_woman)
    TextView tvSexWoman;
    @BindView(R.id.tv_birthday)
    TextView tvBirthday;
    @BindView(R.id.tv_calendar)
    TextView tvCalendar;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.check_box)
    CheckBox checkBox;
    @BindView(R.id.bt_commit)
    Button btCommit;
    private View inflate;

    @Override
    protected View createView(LayoutInflater inflater) {
        inflate = inflater.inflate(R.layout.fragment_give_name, null);
        mIsVisible = true;
        return inflate;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListenter() {
    }

    @Override
    protected void initView() {

    }

    @OnClick({R.id.tv_sex_man, R.id.tv_sex_woman, R.id.tv_birthday, R.id.tv_calendar, R.id.tv_address, R.id.bt_commit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_sex_man:
                tvSexMan.setBackgroundResource(R.drawable.ic_sex_check);
                tvSexWoman.setBackgroundResource(0);
                break;
            case R.id.tv_sex_woman:
                tvSexWoman.setBackgroundResource(R.drawable.ic_sex_check);
                tvSexMan.setBackgroundResource(0);
                break;
            case R.id.tv_birthday:
                break;
            case R.id.tv_calendar:
                break;
            case R.id.tv_address:
                break;
            case R.id.bt_commit:
                ARouter.getInstance().build(RoutConstants.Activity.BE_NAME_ANALYSE_ACTIVITY).navigation();
                break;
        }
    }
}
