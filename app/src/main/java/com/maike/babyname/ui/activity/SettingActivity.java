package com.maike.babyname.ui.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.maike.babyname.R;
import com.maike.babyname.base.BaseActivity;
import com.maike.babyname.manager.RoutConstants;
import com.maike.babyname.view.TopView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 更多设置
 */
@Route(path = RoutConstants.Activity.SETTING_ACTIVITY)
public class SettingActivity extends BaseActivity {

    @BindView(R.id.top_view)
    TopView topView;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_setting;
    }

    @Override
    protected void initView() {
        topView.setTitle(R.string.more_setting);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }
}