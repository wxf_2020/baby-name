package com.maike.babyname.ui.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.maike.babyname.R;
import com.maike.babyname.base.BaseFragment;
import com.maike.babyname.utils.StatusBarUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 主页
 */
public class HomeFragment extends BaseFragment {
    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    @BindView(R.id.tv_give_name)
    TextView tvGiveName;
    @BindView(R.id.tv_conjecture_name)
    TextView tvConjectureName;
    private View inflate;
    private HashMap<String, Fragment> fragmentHashMap = new HashMap<>();
    private List<String> mDataList = new ArrayList<>();
    private HomeAdapter homeAdapter;

    @Override
    protected View createView(LayoutInflater inflater) {
        inflate = inflater.inflate(R.layout.fragment_home, null);
        mIsVisible = true;
        return inflate;
    }

    @Override
    protected void initData() {
        homeAdapter = new HomeAdapter(getChildFragmentManager());
        mViewPager.setAdapter(homeAdapter);

        mViewPager.setOffscreenPageLimit(2);
        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                setFragmentIndicator(position);
            }
        });
        mDataList.clear();
        mDataList.add("取名");
        mDataList.add("测名");
        homeAdapter.notifyDataSetChanged();

    }

    @Override
    protected void initListenter() {

    }

    @Override
    protected void initView() {

    }

    @OnClick({R.id.tv_give_name, R.id.tv_conjecture_name})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_give_name:
                setFragmentIndicator(0);
                break;
            case R.id.tv_conjecture_name:
                setFragmentIndicator(1);
                break;
        }
    }

    public void setFragmentIndicator(int which) {
        switch (which) {
            case 0:
                tvGiveName.setTextColor(0xffffffff);
                tvConjectureName.setTextColor(0xff010101);
                tvGiveName.setBackgroundResource(R.drawable.ic_main_home_bar);
                tvConjectureName.setBackgroundResource(0);
                mViewPager.setCurrentItem(0);
                break;
            case 1:
                tvGiveName.setTextColor(0xff010101);
                tvConjectureName.setTextColor(0xffffffff);
                tvGiveName.setBackgroundResource(0);
                tvConjectureName.setBackgroundResource(R.drawable.ic_main_home_bar);
                mViewPager.setCurrentItem(1);
                break;
        }
    }


    private class HomeAdapter extends FragmentStatePagerAdapter {


        public HomeAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            String model = mDataList.get(position);

            if (null == fragmentHashMap.get(mDataList.get(position))) {
                if (mDataList.get(position).equals("取名")) {
                    GiveNameFragment giveNameFragment = new GiveNameFragment();
                    fragmentHashMap.put(model, giveNameFragment);
                } else {
                    ConjectureFragment conjectureFragment = new ConjectureFragment();
                    fragmentHashMap.put(model, conjectureFragment);
                }
            }

            return fragmentHashMap.get(model);

        }

        @Override
        public int getCount() {
            return mDataList == null ? 0 : mDataList.size();
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }
    }

}
