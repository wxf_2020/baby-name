package com.maike.babyname.ui.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.maike.babyname.R;
import com.maike.babyname.bean.NameBean;

import java.util.List;

public class NameAdapter extends BaseQuickAdapter<NameBean, BaseViewHolder> {
    public NameAdapter(@Nullable List<NameBean> date) {
        super(R.layout.item_name_line3, date);
    }

    @Override
    protected void convert(BaseViewHolder helper, NameBean item) {
        helper.setText(R.id.tv_pinyin, item.getNamePinYin());
        helper.setText(R.id.tv_name, item.getName());
        helper.setText(R.id.tv_five_elements, item.getFiveElements());
    }
}
