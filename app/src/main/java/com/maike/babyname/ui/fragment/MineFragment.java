package com.maike.babyname.ui.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.alibaba.android.arouter.launcher.ARouter;
import com.maike.babyname.R;
import com.maike.babyname.base.BaseFragment;
import com.maike.babyname.manager.RoutConstants;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 我的
 */
public class MineFragment extends BaseFragment {
    @BindView(R.id.iv_header)
    ImageView ivHeader;
    @BindView(R.id.ll_order)
    LinearLayout llOrder;
    @BindView(R.id.ll_help_feed_back)
    LinearLayout llHelpFeedBack;
    @BindView(R.id.ll_share_me)
    LinearLayout llShareMe;
    @BindView(R.id.ll_setting)
    LinearLayout llSetting;
    private View inflate;

    @Override
    protected View createView(LayoutInflater inflater) {
        inflate = inflater.inflate(R.layout.fragment_mine, null);
        mIsVisible = true;
        return inflate;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListenter() {

    }

    @Override
    protected void initView() {

    }

    @OnClick({R.id.ll_order, R.id.ll_help_feed_back, R.id.ll_share_me, R.id.ll_setting})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_order:
                break;
            case R.id.ll_help_feed_back:
                ARouter.getInstance()
                        .build(RoutConstants.Activity.HELP_FEED_BACK_ACTIVITY)
                        .navigation();
                break;
            case R.id.ll_share_me:
                break;
            case R.id.ll_setting:
                ARouter.getInstance()
                        .build(RoutConstants.Activity.SETTING_ACTIVITY)
                        .navigation();
                break;
        }
    }
}
