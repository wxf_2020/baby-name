package com.maike.babyname.ui.adapter;

import androidx.annotation.Nullable;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.maike.babyname.R;
import com.maike.babyname.bean.AnalyseInfo;
import java.util.List;

/**
 * 项目名称：BabyName
 * 类描述：
 * 创建人：wsk
 * 创建时间：2020/10/16 6:48 PM
 * 修改人：wsk
 * 修改时间：2020/10/16 6:48 PM
 * 修改备注：
 *
 * @author wsk
 */
public class YourInfoAdapter extends BaseQuickAdapter<AnalyseInfo, BaseViewHolder> {

    public YourInfoAdapter(@Nullable List<AnalyseInfo> data) {
        super(R.layout.item_your_info, data);
    }


    @Override
    protected void convert(BaseViewHolder helper, AnalyseInfo item) {
        helper.itemView.setTag(item);
        helper.setText(R.id.tv_title,item.title);
        helper.setText(R.id.tv_content,item.content);

    }
}
