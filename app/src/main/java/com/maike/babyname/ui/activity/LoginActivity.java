package com.maike.babyname.ui.activity;

import android.os.Bundle;
import android.widget.Button;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.maike.babyname.R;
import com.maike.babyname.base.BaseActivity;
import com.maike.babyname.manager.RoutConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 登陆页面
 */
@Route(path = RoutConstants.Activity.LOGIN_ACTIVITY)
public class LoginActivity extends BaseActivity {

    @BindView(R.id.bt_login)
    Button btLogin;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }

    @OnClick(R.id.bt_login)
    public void onViewClicked() {
        ARouter.getInstance()
                .build(RoutConstants.Activity.MAIN_ACTIVITY)
                .navigation();
        finish();
    }
}