package com.maike.babyname.ui.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.maike.babyname.R;
import com.maike.babyname.base.BaseListFragment;
import com.maike.babyname.bean.NameBean;
import com.maike.babyname.bean.NameRecommendInfo;
import com.maike.babyname.manager.RoutConstants;
import com.maike.babyname.ui.adapter.NameRecommendAdapter;
import com.maike.babyname.utils.LayoutManagerUtil;
import com.maike.babyname.utils.LinearLayoutColorDivider;
import com.maike.babyname.utils.UiUtils;
import com.maike.babyname.view.dialog.ConJectureNamePayDialog;
import java.util.ArrayList;
import java.util.List;

/**
 * 项目名称：BabyName
 * 类描述：名字推荐
 * 创建人：wsk
 * 创建时间：2020/10/16 6:00 PM
 * 修改人：wsk
 * 修改时间：2020/10/16 6:00 PM
 * 修改备注：
 *
 * @author wsk
 */
@Route(path = RoutConstants.Fragment.NAME_RECOMMEND_FRAGMENT)
public class NameRecommendFragment extends BaseListFragment {
    @BindView(R.id.recyclerView) RecyclerView mRecyclerView;
    private NameRecommendAdapter mNameRecommendAdapter;
    private List<NameRecommendInfo> mAnalyseInfoList;
    private View mView;


    @Override
    protected View createView(LayoutInflater inflater) {
        mView = inflater.inflate(R.layout.fragment_name_recommend, null);
        return mView;
    }


    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return LayoutManagerUtil.getLinearLayoutManager(getContext());
    }


    @Override
    protected void initView() {
        mView.setBackgroundResource(R.color.trans);
    }


    @Override
    protected void initData() {
        super.initData();
        mAnalyseInfoList = new ArrayList<>();
        loadData(false);
        mRecyclerView.addItemDecoration(new LinearLayoutColorDivider(getContext(), R.color.trans, R.dimen.dp_12, LinearLayoutManager.VERTICAL));
        mNameRecommendAdapter
                .setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
                    @Override
                    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                        ARouter.getInstance()
                               .build(RoutConstants.Activity.BE_NAME_DETAILS_ACTIVITY)
                               .navigation();
                    }
                });
    }


    @Override
    protected BaseQuickAdapter getBaseQuickAdapter() {
        mNameRecommendAdapter = new NameRecommendAdapter(mList);
        return mNameRecommendAdapter;
    }


    @Override
    protected void requestData(boolean getMore) {
        List<NameBean> nameBeanList = new ArrayList<>();
        nameBeanList.add(new NameBean("li", "李", "[木]"));
        nameBeanList.add(new NameBean("yi", "意", "[土]"));
        nameBeanList.add(new NameBean("qing", "卿", "[木]"));
        for (int i = 0; i < 2; i++) {
            NameRecommendInfo nameRecommendInfo = new NameRecommendInfo();
            nameRecommendInfo.nameList = nameBeanList;
            nameRecommendInfo.score = 99;
            nameRecommendInfo.content = "意：志向、胸怀、心志、心意，意指诚深谋远虑、积极景区、行成于思。用作人名意指聪明、心思缜密、心怀大志；\n卿：卿相、爵位、官名、称谓，意指相融以沫、慈眉善目、谦虚恭敬。用作人名意指谦虚、谦和、虚心之义；";
            mAnalyseInfoList.add(nameRecommendInfo);
        }
        onDataSuccess(mAnalyseInfoList);
    }


    @Override
    protected View getFooterView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_more, null);
        Button viewById = view.findViewById(R.id.bt_commit);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, UiUtils
                .dp2px(getContext(), 68));
        view.setLayoutParams(layoutParams);
        viewById.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConJectureNamePayDialog conJectureNamePayDialog = new ConJectureNamePayDialog(getContext());
                conJectureNamePayDialog.show();
            }
        });
        return view;
    }


    @Override
    protected void initListenter() {

    }
}
