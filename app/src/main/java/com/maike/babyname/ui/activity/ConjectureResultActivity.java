package com.maike.babyname.ui.activity;

import android.os.Bundle;
import android.widget.Button;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.maike.babyname.R;
import com.maike.babyname.base.BaseActivity;
import com.maike.babyname.bean.NameBean;
import com.maike.babyname.manager.RoutConstants;
import com.maike.babyname.ui.adapter.NameAdapter;
import com.maike.babyname.utils.ToastUtils;
import com.maike.babyname.view.TopView;
import com.maike.babyname.view.dialog.ConJectureNamePayDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path = RoutConstants.Activity.CONJECTURE_RESULT_ACTIVITY)
public class ConjectureResultActivity extends BaseActivity {

    @BindView(R.id.top_view)
    TopView topView;
    @BindView(R.id.bt_commit)
    Button btCommit;
    @BindView(R.id.rv_name_list)
    RecyclerView rvNameList;
    List<NameBean> nameList = new ArrayList<>();
    private NameAdapter nameAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_conjecture_result;
    }

    @Override
    protected void initView() {
        topView.setTitle("测名结果");
        topView.replaceMoreIcon(R.drawable.ic_mine_share);
        nameAdapter = new NameAdapter(nameList);
        rvNameList.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        rvNameList.setAdapter(nameAdapter);
    }

    @Override
    protected void initData() {
        nameList.add(new NameBean("li", "李", "[木]"));
        nameList.add(new NameBean("yi", "意", "[土]"));
        nameList.add(new NameBean("qing", "卿", "[木]"));
        nameAdapter.notifyDataSetChanged();
    }

    @Override
    protected void initListener() {
        topView.showMoreImg(view -> {
            ToastUtils.showShortToast("点击分享");
        });
    }

    @OnClick(R.id.bt_commit)
    public void onViewClicked() {
        ConJectureNamePayDialog conJectureNamePayDialog = new ConJectureNamePayDialog(mContext);
        conJectureNamePayDialog.show();
    }


}