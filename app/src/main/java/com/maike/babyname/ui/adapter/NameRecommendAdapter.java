package com.maike.babyname.ui.adapter;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.maike.babyname.R;
import com.maike.babyname.bean.NameRecommendInfo;
import java.util.List;

/**
 * 项目名称：BabyName
 * 类描述：
 * 创建人：wsk
 * 创建时间：2020/10/17 2:40 PM
 * 修改人：wsk
 * 修改时间：2020/10/17 2:40 PM
 * 修改备注：
 *
 * @author wsk
 */
public class NameRecommendAdapter extends BaseQuickAdapter<NameRecommendInfo, BaseViewHolder> {
    public NameRecommendAdapter(@Nullable List<NameRecommendInfo> data) {
        super(R.layout.item_name_recommend, data);
    }


    @Override
    protected void convert(BaseViewHolder helper, NameRecommendInfo item) {
        helper.itemView.setTag(item);
        helper.setText(R.id.tv_content, item.content);
        helper.addOnClickListener(R.id.tv_see_details);
        RecyclerView recyclerView = helper.getView(R.id.recyclerView);

        NameAdapter nameAdapter = new NameAdapter(item.nameList);
        recyclerView
                .setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        recyclerView.setAdapter(nameAdapter);
    }
}
