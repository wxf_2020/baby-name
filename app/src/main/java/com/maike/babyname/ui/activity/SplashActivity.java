package com.maike.babyname.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.maike.babyname.R;
import com.maike.babyname.base.BaseActivity;
import com.maike.babyname.manager.RoutConstants;
import com.maike.babyname.utils.IntentUtils;
import com.maike.babyname.utils.StatusBarUtil;

/**
 * 引导页
 */
@Route(path = RoutConstants.Activity.SPLASH_ACTIVITY)
public class SplashActivity extends BaseActivity {
//    private boolean isFirstOpen;

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected void initView() {
        StatusBarUtil.setTranslucentStatus(this);
        //解决每次从后台切换到前台都会启动欢迎界面的问题
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }
    }

    @Override
    protected void initData() {

        View view = new View(this);
        setContentView(view);
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                ARouter.getInstance()
                        .build(RoutConstants.Activity.LOGIN_ACTIVITY)
                        .navigation();
                finish();
            }
        }, 1000);
    }

    @Override
    protected void initListener() {

    }
}
