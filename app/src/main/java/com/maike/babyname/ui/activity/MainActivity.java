package com.maike.babyname.ui.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.maike.babyname.R;
import com.maike.babyname.base.BaseActivity;
import com.maike.babyname.manager.AppActivityManager;
import com.maike.babyname.manager.RoutConstants;
import com.maike.babyname.ui.fragment.HomeFragment;
import com.maike.babyname.ui.fragment.MineFragment;
import com.maike.babyname.utils.StatusBarUtil;
import com.maike.babyname.utils.ToastUtils;
import com.maike.babyname.view.NaviTabButton;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 主页
 */
@Route(path = RoutConstants.Activity.MAIN_ACTIVITY)
public class MainActivity extends BaseActivity {
    @BindView(R.id.fragment_group)
    FrameLayout fragmentGroup;
    @BindView(R.id.tabbutton_1)
    NaviTabButton tabbutton1;
    @BindView(R.id.tabbutton_2)
    NaviTabButton tabbutton2;
    @BindView(R.id.ll_group)
    LinearLayout llGroup;
    private NaviTabButton[] mTabButtons;
    private HomeFragment homeFragment;
    private MineFragment mineFragment;
    private Fragment selectFragment;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        homeFragment = new HomeFragment();
        mineFragment = new MineFragment();
        mTabButtons = new NaviTabButton[2];
        mTabButtons[0] = tabbutton1;
        mTabButtons[1] = tabbutton2;

        mTabButtons[0].setTitle(getString(R.string.home));
        mTabButtons[0].setIndex(0);
        mTabButtons[0].setSelectedImage(getResources().getDrawable(R.drawable.ic_nav_home_checked));
        mTabButtons[0].setUnselectedImage(getResources().getDrawable(R.drawable.ic_nav_home));

        mTabButtons[1].setTitle(getString(R.string.mine));
        mTabButtons[1].setIndex(1);
        mTabButtons[1].setSelectedImage(getResources().getDrawable(R.drawable.ic_nav_me_checked));
        mTabButtons[1].setUnselectedImage(getResources().getDrawable(R.drawable.ic_nav_me));
        setFragmentIndicator(0);

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }

    public void setFragmentIndicator(int which) {

        mTabButtons[0].setSelectedButton(false);
        mTabButtons[1].setSelectedButton(false);
        mTabButtons[which].setSelectedButton(true);

        switch (which) {
            case 0:
                StatusBarUtil.setStatuBarAndFontColor(this, getResources().getColor(R.color.color_theme), false);
                switchFragment(homeFragment);
                llGroup.setBackgroundResource(R.drawable.ic_main_home_bg);
                break;
            case 1:
                StatusBarUtil.setStatuBarAndFontColor(this, getResources().getColor(R.color.color_theme), false);
                switchFragment(mineFragment);
                llGroup.setBackgroundResource(R.drawable.ic_main_mine_bg);
                break;
        }
    }

    //切换fragment
    private void switchFragment(Fragment to) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (selectFragment != null) {
            if (selectFragment != to) {
                if (!to.isAdded()) {
                    transaction.hide(selectFragment).add(R.id.fragment_group, to).commitAllowingStateLoss();
                } else {
                    transaction.hide(selectFragment).show(to).commit();
                }
                selectFragment = to;
            }
        } else {
            transaction.add(R.id.fragment_group, to);
            selectFragment = to;
            transaction.commit();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finishThis();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    //双击退出相关
    private boolean mFlag = false;
    private long mTimeout = -1;

    private boolean checkBackAction() {
        //判定时间设为1.5秒
        long time = 1500L;
        boolean flag = mFlag;
        mFlag = true;
        boolean timeout = (mTimeout == -1 || (System.currentTimeMillis() - mTimeout) > time);
        if (mFlag && (mFlag != flag || timeout)) {
            mTimeout = System.currentTimeMillis();
            ToastUtils.showShortToast(getResources().getString(R.string.main_press_again));
            return true;
        }
        return !mFlag;
    }

    private void finishThis() {
        if (!checkBackAction()) {
            //不完全退出程序
//            moveTaskToBack(true);
            AppActivityManager.getInstance().popAllActivity();

        }

    }


}