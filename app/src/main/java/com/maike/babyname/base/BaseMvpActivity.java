package com.maike.babyname.base;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.maike.babyname.R;
import com.maike.babyname.manager.AppActivityManager;
import com.maike.babyname.utils.ToastUtils;
import com.maike.babyname.view.WheelDialog;

import butterknife.ButterKnife;

public abstract class BaseMvpActivity<P extends BasePresenter> extends AppCompatActivity {

    protected P mPresenter;
    protected Activity mContext;
    private WheelDialog mWheelDialog;
    private final int STORAGE_PERMISSIONS_REQUEST_CODE = 200;

    @Override
    protected void onCreate(@Nullable android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        int layoutId = getLayoutId();
        if (layoutId != 0) {
            setContentView(layoutId);
        }
        mContext = this;
        AppActivityManager.getInstance().pushActivity(this);
        ButterKnife.bind(this);
        initView();
        initData();
        initListener();
        autoObtainStoragePermission();


    }

    protected abstract int getLayoutId();

    protected abstract void initView();

    protected abstract void initData();

    protected abstract void initListener();

    /**
     * 动态申请sdcard读写权限 和录音权限
     */
    public void autoObtainStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, STORAGE_PERMISSIONS_REQUEST_CODE);
            } else {

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            //动态申请sdcard读写权限
            case STORAGE_PERMISSIONS_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    ToastUtils.showShortToast("授权请求被拒绝,有可能有些功能无法完成");
                }
                break;
            default:
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null)
            mPresenter.onDestroy();

    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
//        overridePendingTransition(R.anim.actvity_start, R.anim.actvity_exit);
//        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
        overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_left_exit);

    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
////        overridePendingTransition(R.anim.actvity_start, R.anim.actvity_exit);
//        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
////        overridePendingTransition(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_left_exit);
//
//    }

    public void back(android.view.View v) {
        onBackPressed();
    }

    protected void showProgressDialog(String message) {
        if (mWheelDialog == null) {
            mWheelDialog = new WheelDialog(mContext, R.style.WheelDialogTheme);
        }
        if (mWheelDialog.isShowing()) {
            return;
        }
        if (TextUtils.isEmpty(message)) {
            mWheelDialog.setMessage("请稍后..");
        } else {
            mWheelDialog.setMessage(message);
        }
        mWheelDialog.show();
    }

    protected void hideProgressDialog() {
        if (mWheelDialog != null && mWheelDialog.isShowing()) {
            mWheelDialog.dismiss();
        }
    }

    public void onError() {
        hideProgressDialog();
    }

}
