package com.maike.babyname.base;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.maike.babyname.R;
import com.maike.babyname.utils.ToastUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import java.util.ArrayList;
import java.util.List;

/**
 * 项目名称：userchat
 * 类描述：
 * 创建人：wsk
 * 创建时间：2020/3/21 4:54 PM
 * 修改人：wsk
 * 修改时间：2020/3/21 4:54 PM
 * 修改备注：
 *
 * @author wsk
 */
public abstract class BaseListActivity<M,P> extends BaseMvpActivity {
    @Nullable public SmartRefreshLayout mSmartRefreshLayout;
    public RecyclerView mRecycleView;
    public TextView mBack, mTitleText, mRightText;
    protected BaseQuickAdapter mBaseQuickAdapter;

    protected List<M> mList = new ArrayList<>();
    /**
     * 当前页码
     */
    protected int mPage;
    /**
     * 是否能够加载下一页
     */
    protected boolean hasMore = true;
    /**
     * 是否清空列表
     */
    protected boolean isClearList = false;
    /**
     * 当前是否正在加载中
     */
    protected boolean isLoading = false;


    @Override
    public int getLayoutId() {
        return R.layout.activity_refresh_default;
    }


    /**
     * 布局管理
     *
     * @return ""
     */
    protected abstract RecyclerView.LayoutManager getLayoutManager();


    /**
     * 获取列表头部
     */
    protected View getHeaderView() {
        return null;
    }


    /**
     * 获取列表底部
     * @return
     */
    protected View getFooterView() {
        return null;
    }


    /**
     * 获取列表空视图
     */
    protected View getEmptyView() {
        return null;
    }


    /**
     * 请求列表的数据，将数据回调到{{@link #onDataSuccess(List)}}进行列表刷新
     *
     * @param getMore 是否可以加载更多
     */
    protected abstract void requestData(boolean getMore);

    /**
     * 获取adapter
     *
     * @return BaseQuickAdapter
     */
    protected abstract BaseQuickAdapter getBaseQuickAdapter();


    @Override
    protected void initData() {
        mSmartRefreshLayout = findViewById(R.id.smartRefreshLayout);
        mRecycleView = findViewById(R.id.recyclerView);

        if (null != mSmartRefreshLayout) {
            mSmartRefreshLayout.setReboundDuration(50);
            mSmartRefreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                    if (hasMore) {
                        loadData(true);
                    } else {
                        refreshLayout.finishLoadMore();
                        ToastUtils.showShortToast("没数据了");
                    }
                }


                @Override
                public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                    hasMore = true;
                    loadData(false);
                }
            });
        }
        if (null != mBack) {
            mBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
        mRecycleView.setLayoutManager(getLayoutManager());
        mBaseQuickAdapter = getBaseQuickAdapter();
        mRecycleView.setAdapter(mBaseQuickAdapter);
        View view = getHeaderView();
        if (null != view) {
            mBaseQuickAdapter.setHeaderView(view);
        }
        View footerView = getFooterView();
        if (null != footerView) {
            mBaseQuickAdapter.setFooterView(footerView);
        }
        View emptyView = getEmptyView();
        if (emptyView != null) {
            mBaseQuickAdapter.setEmptyView(emptyView);
        }
    }



    protected void loadData(boolean getMore) {
        if (isLoading) {
            return;
        }
        isLoading = true;
        isClearList = !getMore;
        if (getMore) {
            //mPage = mList.size() / Constants.PAGE_LIMIT_15;
            //if (mList.size() % Constants.PAGE_LIMIT_15 != 0) {
            //    mPage += 1;
            //}
            mPage += 1;
        } else {
            mPage = 0;
        }
        requestData(getMore);
    }


    /**
     * requestData方法请求数据成功后将数据集传入本方法进行刷新列表
     */
    protected void onDataSuccess(@Nullable List<M> newData) {
        if (mBaseQuickAdapter == null || mRecycleView == null) {
            return;
        }
        isLoading = false;
        if (isClearList) {
            mList.clear();
            mBaseQuickAdapter.notifyDataSetChanged();
        }
        if (null != mSmartRefreshLayout) {
            mSmartRefreshLayout.finishRefresh();
            mSmartRefreshLayout.finishLoadMore();
        }
        hasMore = checkLoadMore(newData);
        if (null != newData && newData.size() > 0) {
            int positionStart = mList.size();
            mList.addAll(newData);
            mBaseQuickAdapter.notifyItemRangeInserted(
                    positionStart + mBaseQuickAdapter.getHeaderLayoutCount(), newData.size());
        }
    }


    /**
     * 判断是否能够加载下一页
     *
     * @param newData 本次数据源
     */
    protected boolean checkLoadMore(@Nullable List<M> newData) {
        return null != newData && newData.size() >= 10;
    }

}
