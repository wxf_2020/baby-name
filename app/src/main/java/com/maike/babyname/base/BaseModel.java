package com.maike.babyname.base;


import com.maike.babyname.network.HttpMethods;

public class BaseModel<T> {

    protected HttpMethods mHttpMethods;
    protected T mAPI;

    public BaseModel(Class<T> clazz) {
        mHttpMethods = HttpMethods.getInstance();
        mAPI = mHttpMethods.createAPI(clazz);
    }


    public HttpMethods getHttpMethod() {
        return mHttpMethods;
    }


}
