package com.maike.babyname.base;

import android.os.Handler;
import android.os.Message;

import java.lang.ref.SoftReference;

public abstract class BaseHandler<T> extends Handler {

    private SoftReference<T> mActRef;

    public BaseHandler(T act) {
        this.mActRef = new SoftReference<>(act);
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);

        T act = mActRef.get();
        if (act != null) {
            hanlerMessage(msg, act);
        }

    }

    public abstract void hanlerMessage(Message msg, T act);
}
