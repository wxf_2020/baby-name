package com.maike.babyname.base;

import android.app.Activity;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;


public abstract class BasePresenter<V extends Activity, M> {

    protected V mView;
    protected M mModel;
    protected CompositeSubscription mComposeSubscription;

    public BasePresenter(V view) {
        this.mView = view;
        mComposeSubscription = new CompositeSubscription();
    }


    public void addToCompose(Subscription subscription) {
        mComposeSubscription.add(subscription);
    }

    public void onDestroy() {
        mComposeSubscription.unsubscribe();
    }

}
