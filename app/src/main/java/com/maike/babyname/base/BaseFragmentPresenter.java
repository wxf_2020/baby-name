package com.maike.babyname.base;


import androidx.fragment.app.Fragment;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;


public abstract class BaseFragmentPresenter<V extends Fragment, M> {

    protected V mView;
    protected M mModel;
    protected CompositeSubscription mComposeSubscription;

    public BaseFragmentPresenter(V view) {
        this.mView = view;
        mComposeSubscription = new CompositeSubscription();
    }


    public void addToCompose(Subscription subscription) {
        mComposeSubscription.add(subscription);
    }

    public void onDestroy() {
        mComposeSubscription.unsubscribe();
    }

}
